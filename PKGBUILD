# Maintainer: Mark Wagie <mark at manjaro dot org>

pkgname=protontricks
pkgver=1.12.1
pkgrel=1
pkgdesc="A simple wrapper that does winetricks things for Proton enabled games."
arch=('any')
url="https://github.com/Matoking/protontricks"
license=('GPL-3.0-or-later')
depends=(
  'python-pillow'
  'python-setuptools'
  'python-vdf'
  'winetricks'
)
makedepends=(
  'python-build'
  'python-installer'
  'python-setuptools-scm'
  'python-wheel'
)
checkdepends=(
  'appstream'
  'desktop-file-utils'
)
optdepends=(
  'yad: for GUI'
  'zenity: fallback for GUI'
)
source=("$pkgname-$pkgver.tar.gz::$url/archive/refs/tags/$pkgver.tar.gz")
sha256sums=('2f81d2faca7afc9e041c89862b375f660041a35d36554c06ba9d97d9b7ec22fe')

build() {
  cd "$pkgname-$pkgver"
  export SETUPTOOLS_SCM_PRETEND_VERSION=$pkgver
  python -m build --wheel --no-isolation
}

check() {
  cd "$pkgname-$pkgver"
  desktop-file-validate "src/$pkgname/data/share/applications"/*.desktop
  appstreamcli validate --no-net "data/com.github.Matoking.$pkgname.metainfo.xml"
}

package() {
  cd "$pkgname-$pkgver"
  python -m installer --destdir="$pkgdir" dist/*.whl

  # Remove protontricks-desktop-install, since we already install
  # desktop files properly
  rm -v "$pkgdir/usr/bin/$pkgname-desktop-install"
}
